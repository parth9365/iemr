package com.iemr.domain;

public class Doctor {
	
	private double doctorId;
	private double clinicId;
	private String firstName;
	private String lastName;
	private String doctorLoginId;
	private String passwordHash;
	
	public double getDoctorId() {
		return doctorId;
	}
	public void setDoctorId(double doctorId) {
		this.doctorId = doctorId;
	}
	public double getClinicId() {
		return clinicId;
	}
	public void setClinicId(double clinicId) {
		this.clinicId = clinicId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getDoctorLoginId() {
		return doctorLoginId;
	}
	public void setDoctorLoginId(String doctorLoginId) {
		this.doctorLoginId = doctorLoginId;
	}
	public String getPasswordHash() {
		return passwordHash;
	}
	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}
	
}
