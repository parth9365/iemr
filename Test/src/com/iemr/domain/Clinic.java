package com.iemr.domain;

import java.util.List;

public class Clinic {
	
	private double clinicId;
	private String clinicCode;
	private String name;
	private String location;

	public double getClinicId() {
		return clinicId;
	}
	public void setClinicId(double clinicId) {
		this.clinicId = clinicId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getClinicCode() {
		return clinicCode;
	}
	public void setClinicCode(String clinicCode) {
		this.clinicCode = clinicCode;
	}

	public List<Doctor> getAllDoctors(){
		
		return null;
	}
}
